﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneracionProceduralSinPerlin : MonoBehaviour
{
    public int width;
    public int height;
    public int minStonHeight;
    public int maxStonHeight;
    public GameObject dirt;
    public GameObject grass;
    public GameObject stone;

    void Start()
    {
        Generacion();
    }

    private void Generacion()
    {
        Vector2 pos = this.transform.position;

        for (int x = 0; x < width; x++)
        {
            int minHeight = height - 1;
            int maxHeight = height + 2;

            height = UnityEngine.Random.Range(minHeight, maxHeight);

            int minStoneSpawnDistance = height - minStonHeight;
            int maxStoneSpawnDistance = height - maxStonHeight;
            int totalStonDistance = UnityEngine.Random.Range(minStoneSpawnDistance, maxStoneSpawnDistance);

            for (int y = 0; y < height; y++)
            {
                if(y< totalStonDistance)
                {
                    GameObject newStone = Instantiate(stone);
                    newStone.transform.position = new Vector3(pos.x + x, pos.y + y);
                }
                else
                {
                GameObject newDirt = Instantiate(dirt);
                newDirt.transform.position = new Vector3(pos.x + x, pos.y + y);
                }
            }
            if(totalStonDistance == height)
            {
                GameObject newStone = Instantiate(stone);
                newStone.transform.position = new Vector3(pos.x + x, height);
            }
            else
            {
                GameObject newGrass = Instantiate(grass);
                newGrass.transform.position = new Vector3(pos.x + x, height);
            }
        }
    }
}
