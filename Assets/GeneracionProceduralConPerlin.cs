﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GeneracionProceduralConPerlin : MonoBehaviour
{
    [Header("Terrain Gen")]
    public int width; //x
    public int height; //y
    public float smoothness;

    [Header("Cave Gen")]
    [Range(0,1)]
    public float modifier;

    [Header("Tile")]
    public TileBase groundTile;
    public Tilemap groundTilemap;
    public TileBase caveTile;
    public Tilemap caveTilemap;


    int[,] map;

    public float seed;

    void Start()
    {
        Generation();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            print("Recargar mapa");
            Generation();
        }
    }

    void Generation()
    {
        seed += 2;
        clearMap();
        map = GenerateArray(width, height, true);
        map = TerrainGeneration(map);
        RenderMap(map, groundTilemap,caveTilemap, groundTile , caveTile);
    }

    private int[,] GenerateArray(int width , int height , bool empty)
    {
        int[,] map = new int[width, height];
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (empty)
                {
                    map[x, y] = 0;
                } else
                {
                    map[x, y] = 1;
                }                
            }
        }
        return map;
    }

    public int[,] TerrainGeneration(int[,] map)
    {
        int perlinHeight;
        for (int x = 0; x < width; x++)
        {
            perlinHeight = Mathf.RoundToInt(Mathf.PerlinNoise(x / smoothness, seed) * height/2);
            perlinHeight += height / 2;
            for (int y = 0; y < perlinHeight; y++)
            {
                //map[x, y] = 1;
                int caveValue = Mathf.RoundToInt(Mathf.PerlinNoise((x*modifier)+seed, (y * modifier) + seed));
                map[x, y] = (caveValue == 1)? 2 : 1;
            }
        }
        return map;
    }


    public void RenderMap(int[,] map , Tilemap groundTileMap ,Tilemap caveTilemap ,TileBase groundTileBase , TileBase caveTile)
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if(map[x,y] == 1)
                {
                    groundTilemap.SetTile(new Vector3Int(x, y, 0), groundTileBase);
                }
                else if (map[x, y] == 2)
                {
                    caveTilemap.SetTile(new Vector3Int(x, y, 0), caveTile);
                }
            }
        }
    }

    void clearMap()
    {
        groundTilemap.ClearAllTiles();
        caveTilemap.ClearAllTiles();
    }
}

